const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CreateFileWebpack = require("create-file-webpack");
const path = require("path");
const util = require("util");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const rootPath = path.resolve(__dirname, "..");
const buildPath = path.join(rootPath, "build");
const assetsPath = path.join(buildPath, "assets");
const devDependencies = require(path.join(
  rootPath,
  "package.json"
)).devDependencies;

// build sys_info variable
async function sys_info() {
  if (this.sys_info != null) return this.sys_info;
  var gitRepoInfo = require("git-repo-info")();
  this.sys_info = {
    "commit-hash": gitRepoInfo.sha,
    "commit-date": gitRepoInfo.committerDate,
  };
  return this.sys_info;
}

// build version file
async function versionFile() {
  return new CreateFileWebpack({
    content: JSON.stringify(await sys_info(), null, 2),
    fileName: "assets/version",
    path: buildPath,
  });
}

// generate index.html from template src/templates/index.html
async function html() {
  const sysInfo = JSON.stringify(await sys_info());
  return new HtmlWebpackPlugin({
    hash: true,
    sys_info: sysInfo,
    date : new Date().toLocaleDateString('fr-FR'),
    template: "./src/templates/index.html",
    filename: `index.html`,
    publicPath: "",
  });
}

// bundle css
function css() {
  return new MiniCssExtractPlugin({
    filename: "assets/[name].[contenthash].css",
  });
}

function copies() {
  return new CopyPlugin({
    patterns: [
      {
        // htaccess
        from: "./src/.htaccess",
        to: buildPath,
      },
      {
        from: "./src/lib/",
        to:  buildPath + "/lib/",
      }
    ],
  });
}

function jquery() {
  return new webpack.ProvidePlugin({
    $: "jquery",
    jQuery: "jquery",
  });
}

function p5() {
  return new webpack.ProvidePlugin({
    p5: "p5",
  });
}

async function main() {
  return {
    entry: "./src/ts/main.ts",
    output: {
      filename: "assets/[name].[contenthash].js",
      chunkFilename: "assets/[name].[contenthash].js",
      assetModuleFilename: "assets/[hash][ext][query]",
      path: buildPath,
      clean: true,
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, "css-loader"],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif|mp3)$/i,
          type: "asset/resource",
        },
      ],
    },
    resolve: {
      extensions: [".ts", ".js"],
    },
    plugins: [
      await html(),
      css(),
      await versionFile(),
      copies(),
      jquery(),
      p5(),
    ],
    devServer: {
      static: {
        directory: buildPath,
      },
      devMiddleware: {
        writeToDisk: true,
      },
      compress: true,
      port: 8888,
    },
  };
}

module.exports = main;
