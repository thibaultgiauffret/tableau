# Tableau

"Tableau" est une boîte à outils en ligne pour les disciplines scientifiques (principalement à destination de l'enseignement secondaire, mais pas que...). Cette application est vouée à être projetée au tableau ou à être utilisée par les élèves sur leur propre ordinateur/tablette.

Une version en ligne est accessible juste ici : [tableau.ensciences.fr](https://tableau.ensciences.fr) ou là [http://thibgiauffret.frama.io/tableau](http://thibgiauffret.frama.io/tableau).

**🚧 ATTENTION :** Ce projet est en cours de développement. De nombreuses fonctionnalités sont manquantes et des bugs peuvent survenir.

## 🖥 Tester en local

- Installer NodeJS et NPM
- Installer git : [https://git-scm.com/downloads](https://git-scm.com/downloads)
- Cloner le dépôt : `git clone https://framagit.org/ThibGiauffret/tableau.git`
- Installer les dépendances : `npm install`
- Lancer le serveur local de développement : `npm run test`

## 🛠 Contribuer

Vous pouvez contribuer au projet en proposant des améliorations ou des corrections de bugs [juste là](https://framagit.org/ThibGiauffret/tableau/-/issues) ou en proposant des requêtes de fusion [ici](https://framagit.org/ThibGiauffret/tableau/-/merge_requests).

## ⚖ Licence

"Tableau" est sous licence GNU GPL v3. Vous pouvez consulter le texte complet de la licence [ici](https://www.gnu.org/licenses/gpl-3.0.html).