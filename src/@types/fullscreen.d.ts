// Adding types for fullscreen (not clean... but it works)
export interface Document extends HTMLDocument {
    mozCancelFullScreen?: any;
    msExitFullscreen?: any
    webkitExitFullscreen?: any;
    mozFullScreenElement?: Element;
    msFullscreenElement?: Element;
    webkitFullscreenElement?: Element;
}

export interface HTMLElement extends Element {
    msRequestFullscreen?: any
    mozRequestFullscreen?: any
    webkitRequestFullscreen?: any
}
