/*
* gui.ts
* This file contains the GUI class that is responsible for setting up the user interface (UI) of the application. It initializes the different components of the GUI, loads the script to execute, and adds event listeners to the different components. The GUI class is called from the main.ts file, which is the main entry point for the application.
*/

import '@fontsource/open-sans'
import 'bootstrap/dist/css/bootstrap.min.css'
// Popper.js is a dependency of Bootstrap, so it must be loaded before Bootstrap
import $ from 'jquery'
import 'jqueryui'
import 'jquery.nicescroll'
import 'jquery-ui-touch-punch'
import { left } from '@popperjs/core'
import { Popover } from 'bootstrap'
import '@fortawesome/fontawesome-free/css/all.css'
import '../css/style.css'
import { Document, HTMLElement } from '../@types/fullscreen'
import satellite from '../assets/images/satelliteBg.png'
import quadrillageBg from '../assets/images/quadrillageBg.svg'
import pointsBg from '../assets/images/pointsBg.svg'
import lignesBg from '../assets/images/lignesBg.svg'
import quadrillageBgDark from '../assets/images/quadrillageBgDark.svg'
import pointsBgDark from '../assets/images/pointsBgDark.svg'
import lignesBgDark from '../assets/images/lignesBgDark.svg'

// Load tools
import { setUserParams } from './userParams'
import { initClock } from './clock'

// Load applets
import { EmbedViewer } from './applets/embedViewer'
import { FileViewer } from './applets/fileViewer'
import { Chronometer } from './applets/chronometer'
import { Timer } from './applets/timer'
import { QRCodeGenerator } from './applets/qrcode'
import { RandomDraw } from './applets/randomDraw'
import { RandomDice } from './applets/dice'
import { Reactions } from './applets/reactions'
import { TrafficLight } from './applets/trafficLight'
import { SoundLevel } from './applets/soundLevel'
import { Classroom } from './applets/classroom'
import { SummerNote } from './applets/summernote'
import { Drawing } from './applets/drawing'
import { EquationEditor } from './applets/equationEditor'
import { MoleculeEditor } from './applets/moleculeEditor'
import { Mindmap } from './applets/mindmap'
import { Geogebra } from './applets/geogebra'
import { Python } from './applets/python'
import { Calculators } from './applets/calculators'

/*
* GUI class
*/
export default class GUI {
  forceLowRes: boolean
  darkMode: boolean
  scale: number
  background: string
  userParams: any

  public constructor () {
    this.forceLowRes = false
    this.darkMode = false
    this.scale = 1
    this.background = ''
    this.userParams = {}
  }

  /**
     * Initialize the GUI.
     */
  async init () {
    try {
      await this._init()
    } catch (error) {
      console.error(error)
    }
  }

  /**
    * Internal GUI init.
    * It calls setupUI then loadInitialContent.
    */
  async _init () {
    await this.setupUI()
  }

  /**
     * The setupUI method is a protected method of the GUI class that sets up the user interface (UI)
     * @param options
     */
  protected async setupUI () {
    // Store the user parameters in local storage
    if (localStorage.getItem('userParams')) {
      this.userParams = JSON.parse(localStorage.getItem('userParams')!)
    } else {
      localStorage.setItem('userParams', JSON.stringify({}))
    }

    // Get the user parameters
    console.log(this.userParams)
    if (this.userParams.gui) {
      if (this.userParams.gui.forceLowRes) {
        this.forceLowRes = true
      }
      if (this.userParams.gui.darkMode) {
        this.setDarkMode()
      }
      if (this.userParams.gui.background) {
        this.background = this.userParams.gui.background
        this.changeBackground(this.background, '')
      }
    }

    // Handle preloader
    this.handlePreloader()

    // Handle resize
    $(window).on('resize', () => {
      this.handleResize()
    })

    // Display the time and date
    initClock()

    // Add event listeners
    const forceResolutionBtn = document.getElementById('forceResolution')
    forceResolutionBtn!.addEventListener('click', () => {
      this.forceResolution()
    })

    const fullscreenButton = document.getElementById('fullscreen')
    fullscreenButton!.addEventListener('click', this.fullscreen)

    const zoomInButton = document.getElementById('zoomIn')
    zoomInButton!.addEventListener('click', this.zoomIn)

    const zoomOutButton = document.getElementById('zoomOut')
    zoomOutButton!.addEventListener('click', this.zoomOut)

    const backgroundButton = document.getElementsByClassName('changeBackground')
    for (let i = 0; i < backgroundButton.length; i++) {
      backgroundButton[i].addEventListener('click', () => {
        // Get img src in span backgroundButton[i]
        const src = $(backgroundButton[i]).find('img').attr('src')
        const ref = $(backgroundButton[i]).data('bg')
        this.changeBackground(ref!, src!)
      })
    }

    const colorPicker = document.getElementById('colorPicker')
    const color = (colorPicker as HTMLInputElement).value
    $('#colorPickerCover').css('background-color', color)
    colorPicker!.addEventListener('change', () => {
      this.changeBackgroundColorPicker((colorPicker as HTMLInputElement).value)
    })

    // Change background image
    this.changeBackgroundImage()

    // Handle dark mode
    const darkMode = document.getElementById('darkMode')
    darkMode!.addEventListener('click', () => {
      this.setDarkMode()
    })

    // Load the applets
    const embedViewer = new EmbedViewer()
    embedViewer.init()

    const fileViewer = new FileViewer()
    fileViewer.init()

    const chronometer = new Chronometer()
    chronometer.init()

    const timer = new Timer()
    timer.init()

    const qrcode = new QRCodeGenerator()
    qrcode.init()

    const randomDraw = new RandomDraw()
    randomDraw.init()

    const randomDice = new RandomDice()
    randomDice.init()

    const reactions = new Reactions()
    reactions.init()

    const trafficLight = new TrafficLight()
    trafficLight.init()

    const soundLevel = new SoundLevel()
    soundLevel.init()

    const classroom = new Classroom()
    classroom.init()

    const summerNote = new SummerNote()
    summerNote.init()

    const drawing = new Drawing()
    drawing.init()

    const equationEditor = new EquationEditor()
    equationEditor.init()

    const moleculeEditor = new MoleculeEditor()
    moleculeEditor.init()

    const mindmap = new Mindmap()
    mindmap.init()

    const geogebra = new Geogebra()
    geogebra.init()

    const python = new Python()
    python.init()

    const calculators = new Calculators()
    calculators.init()

    // Put #options at the end of its parent
    $('#options').parent().append($('#options'))

    // Init all popovers
    const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]');
    [...popoverTriggerList].map((popoverTriggerEl) => new Popover(popoverTriggerEl))

    // Initialize nicescroll for the toolbar
    $(document).ready(function () {
      $('#toolbar').niceScroll({
        cursorcolor: '#AAA',
        cursorborderradius: '5px',
        cursorwidth: '5px',
        cursorborder: 'none',
        background: undefined,
        autohidemode: false,
        railalign: left,
        railpadding: { top: 10, right: 5, left: 10, bottom: 10 },
        scrollbarid: 'scrollToolbar',
        disablemutationobserver: true
      })

      // On window resize, resize the nicescroll
      $(window).on('resize', function () {
        $('#toolbar').getNiceScroll().resize()
      })
    })

    if (this.forceLowRes) {
      this.forceResolution()
    }
  }

  /*
    * Handle preloader
    */
  protected async handlePreloader () {
    const preloader = $('#page-preloader')
    const spinner = preloader.find('.spinner')
    preloader.delay(350).fadeOut('slow')
    spinner!.fadeOut()

    const windowWidth = $(window).width()
    if (windowWidth! < 820) {
      $('#fullscreenMessage').show()
    }
  }

  /*
    * Handle resize
    * Show message if window width is less than 820
    */
  protected async handleResize () {
    const windowWidth = $(window).width()
    console.log('forceLowRes:' + this.forceLowRes.toString())
    if (windowWidth! < 820 && !this.forceLowRes) {
      $('#fullscreenMessage').show()
    } else {
      $('#fullscreenMessage').hide()
    }
  }

  /*
    * Force resolution when screen is too small
    */
  protected async forceResolution () {
    this.forceLowRes = true
    $('#fullscreenMessage').hide()
    // Store the user parameter
    this.setGuiUserParams()
  }

  /*
    * Handle fullscreen
    */
  //    Types are located in ../@types/fullscreen.d.ts

  protected async fullscreen () {
    if (!(document as Document).fullscreenElement && !(document as Document).webkitFullscreenElement && !(document as Document).mozFullScreenElement && !(document as Document).msFullscreenElement) {
      const elem = document.documentElement
      if (elem !== undefined) {
        if (elem.requestFullscreen) {
          elem.requestFullscreen()
        } else if ((elem as HTMLElement).mozRequestFullscreen) {
          (elem as HTMLElement).mozRequestFullscreen()
        } else if ((elem as HTMLElement).webkitRequestFullscreen) {
          (elem as HTMLElement).webkitRequestFullscreen()
        } else if ((elem as HTMLElement).msRequestFullscreen) {
          (elem as HTMLElement).msRequestFullscreen()
        }
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen()
      } else if ((document as Document).mozCancelFullScreen) {
        (document as Document).mozCancelFullScreen()
      } else if ((document as Document).webkitExitFullscreen) {
        (document as Document).webkitExitFullscreen()
      } else if ((document as Document).msExitFullscreen) {
        (document as Document).msExitFullscreen()
      }
    }
  }

  /*
    * Handle zoom
    * Use of => to keep the context of this !!!
    */
  protected zoomIn = async () => {
    this.scale += 0.1
    this.zoomModal('.draggableModal', this.scale)
  }

  protected zoomOut = async () => {
    this.scale -= 0.1
    this.zoomModal('.draggableModal', this.scale)
  }

  protected async zoomModal (element: string, scale: number) {
    $(element).css('zoom', Number(scale * 100) + '%')
    $(element).css('zoom', scale)
    $(element).css('-moz-transform', 'scale(' + scale + ')')
  }

  /*
    * Handle background change
    */
  protected async changeBackground (image: string, src: string) {
    console.log(image)
    // Add attribute to html
    if (image !== undefined) {
      $('html').attr('data-background', image)
      const type = image.split('-')[0]
      const content = image.split('-')[1]
      if (type === 'repeat') {
        if (content === 'quadrillage') {
          if ($('html').attr('data-bs-theme') === 'dark') {
            src = quadrillageBgDark
          } else {
            src = quadrillageBg
          }
        } else if (content === 'points') {
          if ($('html').attr('data-bs-theme') === 'dark') {
            src = pointsBgDark
          } else {
            src = pointsBg
          }
        } else if (content === 'lignes') {
          if ($('html').attr('data-bs-theme') === 'dark') {
            src = lignesBgDark
          } else {
            src = lignesBg
          }
        }
        $('body').css('background', "url('" + src + "')")
        // $("body").css("background-size", "auto");
        $('body').css('-webkit-background-size', 'auto')
        $('body').css('-moz-background-size', 'auto')
        $('body').css('-o-background-size', 'auto')
        $('body').css('background-repeat', 'repeat')
      } else if (type === 'norepeat') {
        if (src === '') {
          src = satellite
        }
        $('body').css(
          'background',
          "url('" + src + "')  no-repeat center center fixed"
        )
        $('body').css('background-size', 'cover')
        $('body').css('-webkit-background-size', 'cover')
        $('body').css('-moz-background-size', 'cover')
        $('body').css('-o-background-size', 'cover')
        $('body').css('background-repeat', 'no-repeat')
      } else if (type === 'color') {
        $('body').css('background', 'none')
        $('body').css('background-color', content)
      }
      // Save the user parameter
      this.background = image
      this.setGuiUserParams()
    }
  }

  /*
   * Handle background change with color picker
   */

  protected async changeBackgroundColorPicker (color: string) {
    $('body').css('background', 'none')
    $('body').css('background-color', color)
    $('#colorPickerCover').css('background-color', color)
    $('html').attr('data-background', 'color-' + color)
    // Save the user parameter
    this.background = 'color-' + color
    this.setGuiUserParams()
  }

  /*
   * Handle background change with image given by the user
   */
  protected async changeBackgroundImage () {
    const fileInput = document.getElementById('backgroundImageInput') as HTMLInputElement
    fileInput.click()
    fileInput.addEventListener('change', () => {
      const file = fileInput.files![0]
      const reader = new FileReader()
      reader.onload = function (e) {
        $('body').css(
          'background',
          "url('" + e.target!.result + "')  no-repeat center center fixed"
        )
        $('body').css('background-size', 'cover')
        $('body').css('-webkit-background-size', 'cover')
        $('body').css('-moz-background-size', 'cover')
        $('body').css('-o-background-size', 'cover')
        $('body').css('background-repeat', 'no-repeat')
      }
      reader.readAsDataURL(file)
    })
  }

  /*
    * Handle dark mode
    */

  protected async setDarkMode () {
    if ($('html').attr('data-bs-theme') === 'dark') {
      this.darkMode = false
      $('html').attr('data-bs-theme', 'light')
      $('#darkMode').html('<i class="fas fa-moon"></i>')
      // Set the user parameter
      this.setGuiUserParams()
    } else {
      this.darkMode = true
      $('html').attr('data-bs-theme', 'dark')
      $('#darkMode').html('<i class="fas fa-sun"></i>')
      // Set the user parameter
      this.setGuiUserParams()
    }
    // Update the background
    const background = $('html').attr('data-background')
    this.changeBackground(background!, '')
  }

  protected async setGuiUserParams () {
    setUserParams('gui', { forceLowRes: this.forceLowRes, darkMode: this.darkMode, background: this.background })
  }
}
