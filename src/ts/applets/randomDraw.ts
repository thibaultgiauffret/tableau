import { setModal, setButton, addOption } from '../appManagement'
import $ from 'jquery'

export class RandomDraw {
  id: string
  title: string
  icon: string
  color: string
  link: string
  firstRandomName: boolean
  j: number
  random: string[]

  constructor () {
    this.id = 'randomDraw'
    this.title = 'Tirage au sort'
    this.icon = 'fas fa-user-group'
    this.color = '#558BF2'
    this.link = ''
    this.firstRandomName = true
    this.j = 0
    this.random = []

    // Bind this
    this.randomName = this.randomName.bind(this)
    this.getRandomInt = this.getRandomInt.bind(this)
    this.animateRandom = this.animateRandom.bind(this)
    this.resetName = this.resetName.bind(this)
    this.randomList = this.randomList.bind(this)
    this.removeItemAll = this.removeItemAll.bind(this)
  }

  public init () {
    console.log('randomDraw init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '500px', height: '380px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid p-3'
    content.id = 'randomDrawApp'

    const formGroup = document.createElement('div')
    formGroup.className = 'form-group'
    content.appendChild(formGroup)

    const label = document.createElement('label')
    label.setAttribute('for', 'nameList')
    label.innerHTML = 'Veuillez entrer les noms à tirer au sort :'
    formGroup.appendChild(label)

    const textarea = document.createElement('textarea')
    textarea.className = 'form-control'
    textarea.id = 'nameList'
    textarea.rows = 6
    formGroup.appendChild(textarea)

    const formCheck = document.createElement('div')
    formCheck.className = 'form-check'
    content.appendChild(formCheck)

    const input = document.createElement('input')
    input.className = 'form-check-input'
    input.setAttribute('type', 'checkbox')
    input.setAttribute('value', '')
    input.id = 'nameUnique'
    input.checked = true
    formCheck.appendChild(input)

    const label2 = document.createElement('label')
    label2.className = 'form-check-label'
    label2.setAttribute('for', 'nameUnique')
    label2.innerHTML = 'Tirage sans remise'
    formCheck.appendChild(label2)

    const p = document.createElement('p')
    p.id = 'nameResult'
    p.style.textAlign = 'center'
    p.style.fontSize = '26px'
    p.style.fontWeight = '700'
    p.className = 'text-secondary'
    p.innerHTML = 'Résultat'
    content.appendChild(p)

    const div = document.createElement('div')
    div.className = 'd-flex justify-content-center mt-2'
    content.appendChild(div)

    const flexElem1 = document.createElement('div')
    flexElem1.className = 'm-1'
    const button = document.createElement('button')
    button.className = 'btn btn-success'
    button.id = 'nameButton'
    button.innerHTML = '<i class="fa-solid fa-play"></i>'
    flexElem1.appendChild(button)
    div.appendChild(flexElem1)
    button.onclick = this.randomName

    const flexElem2 = document.createElement('div')
    flexElem2.className = 'm-1'
    const button2 = document.createElement('button')
    button2.className = 'btn btn-warning'
    button2.id = 'nameResetButton'
    button2.innerHTML = '<i class="fa-solid fa-arrows-rotate"></i>'
    flexElem2.appendChild(button2)
    div.appendChild(flexElem2)
    button2.onclick = this.resetName

    return content
  }

  private randomName () {
    if (this.firstRandomName) {
      this.random = this.randomList()
      $('#nameButton').html("<i class='fa-solid fa-forward'></i>")
      this.animateRandom(this.random, this.random[this.j])
      this.firstRandomName = false
      if ($('#nameUnique').is(':checked')) {
        this.j += 1
      } else {
        this.j = this.getRandomInt(this.random.length)
      }
    } else if (this.j < this.random.length) {
      this.animateRandom(this.random, this.random[this.j])
      if ($('#nameUnique').is(':checked')) {
        this.j += 1
      } else {
        this.j = this.getRandomInt(this.random.length)
      }
    } else {
      $('#nameButton').html('Tirer au sort à nouveau !')
      this.resetName()
    }
  }

  private getRandomInt (max: number) {
    return Math.floor(Math.random() * max)
  }

  private animateRandom (list: string[], result: string) {
    let counter = 0
    $('#nameResult').removeClass('text-danger')
    const t = setInterval(() => {
      const k = this.getRandomInt(list.length)

      counter += 1
      $('#nameResult').addClass('text-secondary')

      $('#nameResult').html(list[k])
      if (counter >= 8) {
        clearInterval(t)
        $('#nameResult').html(result)
        $('#nameResult').addClass('text-danger')
        // Clear the interval
        clearInterval(t)
      }
    }, 180)
  }

  private resetName () {
    $('#nameResult').html('Résultat')
    $('#nameButton').html("<i class='fas fa-play'></i>")
    $('#nameResult').removeClass('text-danger')
    //   $("#nameList").val("");
    this.firstRandomName = true
    this.random = []
    this.j = 0
  }

  private randomList () {
    const elem = document.getElementById('nameList')! as HTMLTextAreaElement
    const text: string = elem.value
    const words = Array.from(new Set(text.split(/\r?\n/)))
    const random = []

    for (let i = 0; i < 5; i++) {
      const rn = Math.floor(Math.random() * words.length)
      random.push(words[rn])
      words.splice(rn, 1)
    }

    const newRandom = this.removeItemAll(random, '')

    return this.removeItemAll(newRandom, '')
  }

  private removeItemAll (arr: string[], value: string) {
    let i = 0
    while (i < arr.length) {
      if (arr[i] === value) {
        arr.splice(i, 1)
      } else {
        ++i
      }
    }
    return arr
  }
}
