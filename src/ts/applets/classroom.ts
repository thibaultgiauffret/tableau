import { setModal, setButton, addOption } from '../appManagement'
import { ClassroomPlan } from '../../lib/plan_de_classe/main'

export class Classroom {
  id: string
  title: string
  icon: string
  color: string
  link: string

  public constructor () {
    this.id = 'classroom'
    this.title = 'Plan de classe'
    this.icon = 'fa-solid fa-chair'
    this.color = '#558BF2'
    this.link = ''
  }

  public init () {
    console.log('classroom init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '1070px', height: '670px' })
    addOption(this.id, this.title)

    const classroomPlan = new ClassroomPlan(document.getElementById('classroomApp')!)
    classroomPlan.init()
  }

  private content () {
    const content = document.createElement('div')
    content.style.position = 'relative'
    content.style.height = '100%'
    content.style.width = '100%'
    content.id = 'classroomApp'

    return content
  }
}
