import { setModal, setButton, addOption } from '../appManagement'
import QRCode from 'qrcode'
import '../../css/qrcode.css'
import $ from 'jquery'

export class QRCodeGenerator {
  id: string
  title: string
  icon: string
  color: string
  link: string
  qrcode: any

  public constructor () {
    this.id = 'qrcode'
    this.title = 'Code QR'
    this.icon = 'fas fa-qrcode'
    this.color = '#558BF2'
    this.link = ''

    this.makeCode = this.makeCode.bind(this)
  }

  public init () {
    console.log('qrcode init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '500px', height: '500px' })
    addOption(this.id, this.title)

    // Fix the modal position (under the backdrop)
    $('#qrcodeSettings').appendTo('body')

    this.makeCode()

    const that = this

    // On #qrcodeApp resize (not ideal but works for now)
    const qrcodeApp = document.getElementById('qrcodeApp')!

    let debounceTimeout: number | null = null
    const observer = new ResizeObserver(() => {
      if (debounceTimeout !== null) {
        clearTimeout(debounceTimeout)
      }

      debounceTimeout = window.setTimeout(() => {
        observer.unobserve(qrcodeApp)
        // Get the #qrcodeApp width and height
        const width = document.getElementById('qrcodeApp')!.offsetWidth
        const height = document.getElementById('qrcodeApp')!.offsetHeight - 80
        const qrcodeSize = Math.min(width, height)
        document.getElementById('qrcodeCanvas')!.style.width = qrcodeSize + 'px'
        document.getElementById('qrcodeCanvas')!.style.height = qrcodeSize + 'px'
        observer.observe(qrcodeApp)
      }, 100)
    })
    observer.observe(qrcodeApp)

    // Update the QRCode on text change
    $('#text')
      .on('blur', function (this: any) {
        that.makeCode()
      })
      .on('keydown', function (e: any) {
        if (e.keyCode === 13) {
          that.makeCode()
        }
      })

    this.updateTheme()
    // Update the colors on #darkMode
    $('#darkMode').on('click', () => {
      this.updateTheme()
    })
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid p-0'
    content.id = 'qrcodeApp'

    // Settings
    const settings = document.createElement('div')
    settings.className = 'input-group p-3'
    const input = document.createElement('input')
    input.type = 'text'
    input.id = 'text'
    input.className = 'form-control form-control-sm'
    input.placeholder = 'https://ensciences.fr'
    input.value = 'https://ensciences.fr'
    input.setAttribute('aria-label', 'https://ensciences.fr')
    input.setAttribute('aria-describedby', 'button-addon2')
    settings.appendChild(input)
    const gear = document.createElement('button')
    gear.type = 'button'
    gear.className = 'btn btn-secondary'
    gear.setAttribute('data-bs-toggle', 'modal')
    gear.setAttribute('data-bs-target', '#qrcodeSettings')
    const gearIcon = document.createElement('i')
    gearIcon.className = 'fa-solid fa-gear'
    gear.appendChild(gearIcon)
    settings.appendChild(gear)
    const rotate = document.createElement('button')
    rotate.type = 'button'
    rotate.className = 'btn btn-success'
    rotate.id = 'button-addon2'
    rotate.onclick = this.makeCode
    const rotateIcon = document.createElement('i')
    rotateIcon.className = 'fa-solid fa-arrows-rotate'
    rotate.appendChild(rotateIcon)
    settings.appendChild(rotate)
    const download = document.createElement('button')
    download.type = 'button'
    download.className = 'btn btn-danger'
    download.id = 'button-addon3'
    download.onclick = this.download
    const downloadIcon = document.createElement('i')
    downloadIcon.className = 'fa-solid fa-download'
    download.appendChild(downloadIcon)
    settings.appendChild(download)
    content.appendChild(settings)

    // QR Code
    const qrcode = document.createElement('canvas')
    qrcode.id = 'qrcodeCanvas'
    qrcode.className = 'w-80'
    content.appendChild(qrcode)

    // Settings modal
    const modal = document.createElement('div')
    modal.className = 'modal'
    modal.id = 'qrcodeSettings'
    modal.setAttribute('tabindex', '-1')
    modal.setAttribute('aria-labelledby', 'qrcodeSettingsLabel')
    // modal.setAttribute('aria-hidden', 'true');
    content.appendChild(modal)

    const modalDialog = document.createElement('div')
    modalDialog.className = 'modal-dialog'
    modal.appendChild(modalDialog)

    const modalContent = document.createElement('div')
    modalContent.className = 'modal-content'
    modalDialog.appendChild(modalContent)

    modalContent.innerHTML = `
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                <i class="fa-solid fa-gear"></i>&nbsp;Paramètres du code QR
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p><strong>Correction d'erreurs :</strong></p>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="low" checked />
                <label class="form-check-label" for="low"> Basse (7%) </label>
                </div>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="medium" />
                <label class="form-check-label" for="medium">
                    Moyenne (15%)
                </label>
                </div>

                <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="high" />
                <label class="form-check-label" for="high"> Haute (30%) </label>
                </div>
                <br />
                <p><strong>Personnalisation :</strong></p>
                <div class="row">
                <div class="col-6">
                    <label for="colorDark" class="form-label">Couleur du code</label>
                    <input type="color" class="form-control form-control-color" id="colorDark" value="#000"
                    title="Choisir la couleur" />
                </div>
                <div class="col-6">
                    <label for="colorLight" class="form-label">Couleur du fond
                    </label>
                    <input type="color" class="form-control form-control-color" id="colorLight" value="#ffffff"
                    title="Choisir la couleur" />
                </div>
                </div>

                <div class="alert alert-info mt-2" role="alert">
                <i class="fa-solid fa-circle-exclamation"></i>&nbsp;Préférer une
                couleur claire pour le fond et sombre pour le code afin que le
                contraste soit suffisamment élevé !
                </div>
            </div>
        `

    const modalFooter = document.createElement('div')
    modalFooter.className = 'modal-footer'
    const close = document.createElement('button')
    close.type = 'button'
    close.className = 'btn btn-secondary'
    close.setAttribute('data-bs-dismiss', 'modal')
    close.innerHTML = 'Fermer'
    modalFooter.appendChild(close)
    const apply = document.createElement('button')
    apply.type = 'button'
    apply.className = 'btn btn-primary'
    apply.setAttribute('data-bs-dismiss', 'modal')
    apply.id = 'validateQRCodeSettings'
    apply.innerHTML = 'Appliquer'
    modalFooter.appendChild(apply)
    modalContent.appendChild(modalFooter)

    apply.onclick = this.makeCode

    return content
  }

  private testCorrection (): any {
    const low = document.getElementById('low') as HTMLInputElement
    const medium = document.getElementById('medium') as HTMLInputElement

    if (low.checked) {
      return 'L'
    } else if (medium.checked) {
      return 'M'
    } else {
      return 'H'
    }
  }

  private makeCode () {
    // Get the elements
    const elText = document.getElementById('text') as HTMLInputElement
    const colorDark = document.getElementById('colorDark') as HTMLInputElement
    const colorLight = document.getElementById('colorLight') as HTMLInputElement

    // Get the text
    if (!elText.value) {
      alert('Input a text')
      elText.focus()
      return
    }

    // Get the correction level
    const correction = this.testCorrection()

    // Get the #qrcodeApp width and height
    const width = document.getElementById('qrcodeApp')!.offsetWidth
    const height = document.getElementById('qrcodeApp')!.offsetHeight - 110
    const qrcodeSize = Math.min(width, height)

    // Add the QRCode
    this.qrcode = QRCode.toCanvas(document.getElementById('qrcodeCanvas'), elText.value, {
      width: 1000,
      color: {
        dark: colorDark.value,
        light: colorLight.value
      },
      errorCorrectionLevel: correction,
      margin: 0
      // errorCorrectionLevel: this.testCorrection()
    }, function (error) {
      if (error) console.error(error)
      document.getElementById('qrcodeCanvas')!.style.width = qrcodeSize + 'px'
      document.getElementById('qrcodeCanvas')!.style.height = qrcodeSize + 'px'
    })
  }

  private download () {
    const canvas = document.getElementById('qrcodeCanvas') as HTMLCanvasElement
    const image = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream')
    const link = document.createElement('a')
    link.setAttribute('href', image)
    link.setAttribute('download', 'qrcode.png')
    link.click()
  }

  private updateTheme () {
    if ($('html').attr('data-bs-theme') === 'dark') {
      $('#colorLight').val('#212529')
      $('#colorDark').val('#ffffff')
    } else {
      $('#colorLight').val('#ffffff')
      $('#colorDark').val('#000000')
    }
    this.makeCode()
  }
}
