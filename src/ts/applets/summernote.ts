import { setModal, setButton, addOption } from '../appManagement'

export class SummerNote {
  id: string
  title: string
  icon: string
  color: string
  link: string

  public constructor () {
    this.id = 'summerNote'
    this.title = 'Note'
    this.icon = 'fas fa-sticky-note'
    this.color = '#558BF2'
    this.link = ''
  }

  public init () {
    console.log('summerNote init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '500px', height: '400px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'w-100'
    content.id = 'summerNoteApp'

    // Iframe
    const iframe = document.createElement('iframe')
    iframe.style.width = '100%'
    iframe.style.height = '100%'
    iframe.style.border = 'none'
    iframe.src = './lib/summernote/index.html'
    content.appendChild(iframe)

    return content
  }
}
