import { setModal, setButton, addOption } from '../appManagement'
import '../../css/trafficLight.css'
import $ from 'jquery'

export class TrafficLight {
  id: string
  title: string
  icon: string
  color: string
  link: string

  constructor () {
    this.id = 'trafficLight'
    this.title = 'Feu tricolore'
    this.icon = 'fas fa-traffic-light'
    this.color = '#558BF2'
    this.link = ''
  }

  public init () {
    console.log('trafficLight init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '240px', height: '315px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid mx-auto mt-2'
    content.id = 'trafficLightApp'

    const span1 = document.createElement('span')
    span1.className = 'redLight'
    span1.id = 'redLight'
    content.appendChild(span1)
    span1.addEventListener('click', function () {
      $('#redLight').addClass('redLight')
      $('#yellowLight').removeClass('yellowLight')
      $('#greenLight').removeClass('greenLight')
    })

    const span2 = document.createElement('span')
    span2.className = 'yellowLight'
    span2.id = 'yellowLight'
    content.appendChild(span2)
    span2.addEventListener('click', function () {
      $('#redLight').removeClass('redLight')
      $('#yellowLight').addClass('yellowLight')
      $('#greenLight').removeClass('greenLight')
    })

    const span3 = document.createElement('span')
    span3.className = 'greenLight'
    span3.id = 'greenLight'
    content.appendChild(span3)
    span3.addEventListener('click', function () {
      $('#redLight').removeClass('redLight')
      $('#yellowLight').removeClass('yellowLight')
      $('#greenLight').addClass('greenLight')
    })

    return content
  }
}
