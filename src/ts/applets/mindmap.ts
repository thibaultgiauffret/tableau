import { setModal, setButton, addOption } from '../appManagement'

export class Mindmap {
  id: string
  title: string
  icon: string
  color: string
  link: string

  public constructor () {
    this.id = 'mindmap'
    this.title = 'Carte mentale'
    this.icon = 'fa-solid fa-lightbulb'
    this.color = '#558BF2'
    this.link = 'https://www.mindmap.ensciences.fr'
  }

  public init () {
    console.log('mindmap init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '800px', height: '600px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.position = 'relative'
    content.style.height = '100%'
    content.style.width = '100%'
    content.id = 'mindmapApp'

    // Iframe
    const iframe = document.createElement('iframe')
    iframe.style.width = '100%'
    iframe.style.height = '100%'
    iframe.style.border = 'none'
    iframe.src = this.link
    iframe.id = 'mindmapIframe'
    content.appendChild(iframe)

    return content
  }
}
