import { setModal, setButton, addOption } from '../appManagement'
import '../../css/soundLevel.css'
import p5 from 'p5'
import 'p5/lib/addons/p5.sound'
import $ from 'jquery'

export class SoundLevel {
  id: string
  title: string
  icon: string
  color: string
  link: string
  levelLimit: number
  recording: boolean
  calculator: number[]
  audioP5: any

  constructor () {
    this.id = 'soundLevel'
    this.title = 'Niveau sonore'
    this.icon = 'fas fa-ear-listen'
    this.color = '#558BF2'
    this.link = ''
    this.levelLimit = 60
    this.recording = false
    this.calculator = []
    this.audioP5 = null
  }

  public init () {
    console.log('soundLevel init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '400px', height: '220px' })
    addOption(this.id, this.title)

    this.setLevel(3)
    $('#values').hide()
    $('#bar').width('0%')
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid mx-auto mt-2'
    content.id = 'soundLevelApp'

    const meter = document.createElement('div')
    meter.className = 'meter'
    content.appendChild(meter)

    const conteneur = document.createElement('div')
    conteneur.className = 'conteneur'
    meter.appendChild(conteneur)

    const bar = document.createElement('div')
    bar.id = 'bar'
    conteneur.appendChild(bar)

    const dFlex = document.createElement('div')
    dFlex.className = 'd-flex justify-content-center mt-2'
    content.appendChild(dFlex)

    const pValues = document.createElement('p')
    pValues.id = 'values'
    dFlex.appendChild(pValues)

    const spanMedian = document.createElement('span')
    spanMedian.id = 'median'
    pValues.appendChild(spanMedian)

    const spanMax = document.createElement('span')
    spanMax.id = 'max'
    pValues.appendChild(spanMax)

    const dFlex2 = document.createElement('div')
    dFlex2.className = 'd-flex justify-content-center mt-2'
    content.appendChild(dFlex2)

    const btnStart = document.createElement('button')
    btnStart.className = 'btn btn-sm btn-success me-2'
    btnStart.id = 'start'
    dFlex2.appendChild(btnStart)

    const i = document.createElement('i')
    i.className = 'fa-solid fa-circle'
    btnStart.appendChild(i)

    btnStart.addEventListener('click', () => {
      this.start()
    })

    const divBtnGroup = document.createElement('div')
    divBtnGroup.className = 'btn-group'
    divBtnGroup.setAttribute('role', 'group')
    divBtnGroup.setAttribute('aria-label', 'Basic example')
    dFlex2.appendChild(divBtnGroup)

    const btn1 = document.createElement('button')
    btn1.className = 'btn btn-sm btn-primary'
    btn1.setAttribute('type', 'button')
    btn1.addEventListener('click', () => {
      this.setLevel(0.5)
    })
    btn1.textContent = '1'
    divBtnGroup.appendChild(btn1)

    const btn2 = document.createElement('button')
    btn2.className = 'btn btn-sm btn-primary'
    btn2.setAttribute('type', 'button')
    btn2.addEventListener('click', () => {
      this.setLevel(1)
    })
    btn2.textContent = '2'
    divBtnGroup.appendChild(btn2)

    const btn3 = document.createElement('button')
    btn3.className = 'btn btn-sm btn-primary'
    btn3.setAttribute('type', 'button')
    btn3.addEventListener('click', () => {
      this.setLevel(2)
    })
    btn3.textContent = '3'
    divBtnGroup.appendChild(btn3)

    const btn4 = document.createElement('button')
    btn4.className = 'btn btn-sm btn-primary'
    btn4.setAttribute('type', 'button')
    btn4.addEventListener('click', () => {
      this.setLevel(3)
    })
    btn4.textContent = '4'
    divBtnGroup.appendChild(btn4)

    const btn5 = document.createElement('button')
    btn5.className = 'btn btn-sm btn-primary'
    btn5.setAttribute('type', 'button')
    btn5.addEventListener('click', () => {
      this.setLevel(4)
    })
    btn5.textContent = '5'
    divBtnGroup.appendChild(btn5)

    return content
  }

  private setLevel (val: number) {
    this.levelLimit = val * 20
    $('#max').html(' (' + Number(this.levelLimit) + '% à ne pas dépasser)')
  }

  private start () {
    if (this.recording) {
      $('#start').addClass('btn-success')
      $('#start').removeClass('btn-danger')
      $('#start').html('<i class="fa-solid fa-circle"></i>')
      this.recording = false

      // Stop the sound level measurement
      this.audioP5.remove()
    } else {
      this.calculator = []
      $('#start').removeClass('btn-success')
      $('#start').addClass('btn-danger')
      $('#start').html('<i class="fa-solid fa-pause"></i>')
      $('#values').show()
      $('#max').html(' (' + Number(this.levelLimit) + '% à ne pas dépasser)')
      this.recording = true

      // Measure the sound level in real time
      const sketch = (p: p5) => {
        let mic: p5.AudioIn
        let fft: p5.FFT
        let level: number

        p.setup = () => {
          p.createCanvas(400, 200)
          mic = new p5.AudioIn()
          mic.start()
          fft = new p5.FFT()
          fft.setInput(mic)
        }

        p.draw = () => {
          level = mic.getLevel()

          const bar = $('#bar')
          bar.width(level * 100 + '%')
          if (level * 100 >= 0.8 * this.levelLimit) {
            bar.css('background-color', 'red')
          } else if (level * 100 >= 0.5 * this.levelLimit) {
            bar.css('background-color', 'orange')
          } else {
            bar.css('background-color', 'green')
          }

          if (this.recording) {
            this.calculator.push(level)
            if (this.calculator.length > 3000) {
              this.calculator.shift()
            }
            $('#median').html(Number(Math.round(this.median(this.calculator) * 100)) + '%')
          }
        }
      }

      // eslint-disable-next-line new-cap
      this.audioP5 = new p5(sketch)
    }
  }

  private median (values: number[]) {
    values.sort(function (a, b) {
      return a - b
    })

    const half = Math.floor(values.length / 2)

    if (values.length % 2) return values[half]
    else return (values[half - 1] + values[half]) / 2.0
  }
}
