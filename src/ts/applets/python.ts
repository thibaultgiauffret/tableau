import { setModal, setButton, addOption } from '../appManagement'

export class Python {
  id: string
  title: string
  icon: string
  color: string
  link: string

  public constructor () {
    this.id = 'python'
    this.title = 'Console Python'
    this.icon = 'fa-brands fa-python'
    this.color = '#558BF2'
    this.link = 'https://www.ensciences.fr/addons/basthon/console/'
  }

  public init () {
    console.log('mindmap init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '800px', height: '600px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.position = 'relative'
    content.style.height = '100%'
    content.style.width = '100%'
    content.id = 'pythonApp'

    // Iframe
    const iframe = document.createElement('iframe')
    iframe.style.width = '100%'
    iframe.style.height = '100%'
    iframe.style.border = 'none'
    iframe.src = this.link
    iframe.id = 'pythonIframe'
    content.appendChild(iframe)

    return content
  }
}
